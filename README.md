RateApi, PHP HTTP client
=======================


Guzzle is a PHP HTTP client that makes it easy to send HTTP requests and
trivial to integrate with web services.

- Simple interface for building query strings, POST requests, streaming large
  uploads, streaming large downloads, using HTTP cookies, uploading JSON data,
  etc...
- Uses PSR-7 interfaces for requests, responses, and streams. This allows you
  to utilize other PSR-7 compatible libraries with Guzzle.

```php

use Rateapi\Api\RestApi;

$header = 	['Content-Type' => 'application/json',
               'platform'=>isset($requestHeader['platform']) ? $requestHeader['platform'] : 'web',
               'timezone'=>isset($requestHeader['timezone']) ? $requestHeader['timezone'] : 'abc',
               'token'=>isset($requestHeader['token']) ? $requestHeader['token'] : getLoginUserToken(),
            ];
$param = ['id'=>1, 'name'=> 'Yagnik Patel'];

$client = new RestApi("https://github.com/yagnikdevani/larademo",$header,$param);
$response = $client->request('GET', 'https://api.github.com/repos/guzzle/guzzle');

return $response; // Get Full Responce in json formate with api status code
```

## Help and docs

- [Stack Overflow](https://stackoverflow.com/users/4845548/yagnik-devani)
- [Gitter](https://github.com/yagnikdevani/)


## Installing RateApi

The recommended way to install Guzzle is through
[Composer](http://getcomposer.org).

run the Composer command to install the latest stable version of Guzzle:

```bash
composer require rateapi/api
```

After installing, you need to require Composer's autoloader:


You can then later update Guzzle using composer:

 ```bash
composer update
 ```