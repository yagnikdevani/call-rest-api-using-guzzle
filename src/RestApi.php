<?php

namespace Rateapi\Api;
use \GuzzleHttp\Client;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Log;
use Config;
/**
 * 
 */
class RestApi
{
	
	function __construct($url, $header , $param)
	{
		$this->url    = $url;	
        $this->header = $header;
        $this->param  = $param;
        $this->client = new Client();

	}

	public function getRequest(){

        $this->validateUrl($this->url);

        try {
            $response   = $this->client->get($this->url, [
                "body" => json_encode($this->param),
                'headers' => $this->header
            ]);

            $data       = json_decode((string) $response->getBody());

            $data->code = $response->getStatusCode();

            if (Config::get('app.api_log') == true) {
                $this->createLog($data);
            }
            
            return $data;

        } catch (GuzzleHttp\Exception\ClientException $e) {
            
            $response               = $e->getResponse();
            $responseBodyAsString   = $response->getBody()->getContents();
            $data                   = json_decode($responseBodyAsString);
            $data->code             = $response->getStatusCode();

            if (Config::get('app.api_log') == true) {
                $this->createLog($data);
            }
            return $data;
        }
	}

    public function postRequest(){

        $this->validateUrl();

        try {

            $response   = $this->client->post($this->url, [
                'body'    => json_encode($this->param),
                'headers' => $this->header
            ]);

            $data       = json_decode((string) $response->getBody());
            $data->code = $response->getStatusCode();

            $this->createLog($data);
                        
            return $data;

        } catch (GuzzleHttp\Exception\ClientException $e) {
            $response   = $e->getResponse();
            $data       = json_decode($response->getBody()->getContents());
            $data->code = $response->getStatusCode();

            $this->createLog($data);
            
            return $data;
        }

    }

    public function validateUrl(){
        
        if (empty($this->url)) {
            return response()->json(array('code'=>400,'msg'=> 'Please Provide Valid request url'));
        }

    }

    public function createLog($data){

        if (Config::get('app.api_log') == true) {

            $log = ['url'       =>  $this->url,
                'header'    =>  $this->header,
                'request_body'  =>    $this->param,
                'response'=>$data,
                'responce_code' =>  $data->code
                ];

            Log::info(json_encode($log,JSON_PRETTY_PRINT));
        }
    }
}